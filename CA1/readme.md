# CA 1 - Version Control

Version control is a class of systems responsible for managing changes to computer programs, documents, large websites, or other collections of information. 

One of the most famous pieces of software used in version control is *GIT*. 

In this class we will demonstrate de workflow and some basic *GIT* commands. Doing so, you will be able to coordinate and work collaboratively with other developers.

After the *GIT* demonstration we will repeat all the exercises with *FOSSIL* - another version control software.

--------------

To begin this assignment we need some project to work on. In our case, it will be this [project](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic/).

#### 1 - Clone the Project

1.1 -The first step is to clone the project to your local repository. You can do this writing the next command in the command line inside the directory where you want to save it:

````
git clone https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic/
````

#### 2 - Create your own remote repository

 2.1 - Go to Bitbucket and create your own remote repository. The name should be 'devops-21-22-1211771'.

 2.2 - Give admin rights to atb@isep.ipp.pt so that your teacher can have access.
 
#### 3 - Create your own local repository

(This will be our first issue. Before you do the next steps, create an issue in Bitbucket (this way you can keep track of your work))
Repeat this for all the exercises.

3.1 You can do this writing the next command in the command line inside the directory where you want to save it (in this case name it *CA1*):

````
git init
````

or by cloning the remote repository created in the directory CA1, as explained before. 
After this steps your local and remote repositories are in sync. 

3.2 Copy all the files but .git from the Project that you cloned and paste them in CA1. 
With the command 

````
git status
````

you can see that your local repository has a some files that are not yet on our remote repository

####4 - Time for your first commit

4.1 Before you commit to the changes you need to add them to your staging area. Use the command

````
git add {file_name}
````
to add what you want to the staging area. If you want to add all the files that you altered, or all the new files, use the command

````
git add .
````
Now that we have all our files in the staging area, you cann Commit the change with the command

````
git commit -m "{message}"
````

The -m stands for the message that you want to associate the commit to. At the same time, we can close the issue created before. To do this, inside the message, write "closes #1", as the issue created was #1. Example:

````
git commit -m "Creates CA1 directory with all respective files closes #1"
````

There are another command you can do with issues. Check them out.

####5 - Sync your remote repository with your local repository

To do this use the command 

````
git push origin master
````

'Origin', stands for the name of your remote repository and 'Master', the branch that you are working on. Don't worry, we will talk about branches later.

Now your repositories are in sync. You can check this by using the 'git status' command.

In the case that your remote repository has some modification that are not yet on you local repository, you can sync them by using the pull command

````
git pull
````

Now that you know the basic sync commands, try to create a readme.md file and add it to your remote repository. Don't forget the sequence: Pull -> Do your work -> Add -> Commit -> Push

####5 - Tags

This is the first version of your software. To mark it, we can use a tag. A tag is like a name. This way we know how to reach it easily. To create a tag use the command:

````
git tag v1.1.0
````

You have created a tag. Now you need to push it to the remote repository. To do this, use the command

````
git push --tags
````

The tag will be associated to the last commit. You can delete a tag with the command:

````
$ git push --delete origin v1.1.0
````

There are other commands related to tags. Experiment on your own.

####5 - Let's practice - modify the source code of the project

For this example add jobYears field to employee. In this case, the jobYears shall be the difference between today and the first year, so make sure to implement that. After that, made some validations, test it and debug it. When all is done sync it with remote repository and add the tag v.1.20

````
git add .
git commit -m "adds jobyears"
git push origin master
git tag v1.2.0
git push --tags
````

Well done! This was the first part of our assignment. Mark your repository with the tag ca1-part1

````
git tag ca1-part1
git push --tags
````

####6 Branches

Let's start the second part of our assignment. From now on we will use branches. This way another developer can continue to work on the main project while we also work on it. To create a branch use the command 

````
git branch {branch_name}
````

In this case, the name of our new branch will be *email-field*. The branch is created, Now we need to start working on it. But before that, we need to 'go' to our new branch. To do that, use the command:

````
git checkout {branch_name}
````

or, in this case, 

````
git checkout email-field
````
 Now we can start doing our modifications to the source code. Add a new field, called email, do the validations, tests and debugs. After all is done we need to merge it with our master branch to do that, use the commands

````
git checkout master
git merge email-field
````

Pay attention -- we need to be in our master branch so that we can merge our modifications. What we did, is called a fast-forward merge. The master branch didn't suffer any modifications, so when we merge our branches, the branch email-field automatically disappeared. Now let's add a tag

````
git tag v1.3.0
git push --tags
````

Let's do it again, to be sure we understood it. This time add a new validation to email. First of, create the branch, do the modifications, test it, debug it and then merge

````
git branch fix-invalid-email
git checkout fix-invalid-email
'change source code and push it to the new branch'
git checkout master
git merge fix-invalid-email
git tag v1.3.1
git push --tags
````
That's it. We finished the second part of our assignment. Add a new tag

````
git tag ca1-part2
git push --tags
````
Congratulations!

# FOSSIl - Implementation

Like GIT, FOSSIl a simple, high-reliability, distributed software configuration management system. As you will see, the commands are very similar, with only some differences. We will compare the two Version Control Pieces of software  in the next exercises and at the end of this file. Right now, lets do the exercises:

Important Note: We couldn't create a sharable remote repository. All the demonstrations will be done in print screens.


####1 - Installing Fossil

Go to [Fossil](https://www2.fossil-scm.org/home/uv/fossil-w64-20220117.zip), and install it.

####2 - Create a local repository

Create a new local repository with the command:

    fossil init repository-filename 

Like, GIT, you can also clone an already existing repository with the command:

    Fossil clone 'URL'

Fossil also gives you the ability to import from another version control software, like GIT.

To work on a project in fossil, you need to check out a local copy of the source tree. Create the directory you want to be the root of your tree and cd into that directory. Then do this: 

    Fossil open repository-filename 

![](Final fossil/1 Create Local Repository.jpg)

Now we are ready to start working. 

3.2 Copy all the files from the Project that you cloned.
With the command

````
Fossil status
````
you can see that your local repository has a some new files

####3 Time for your first commit

4.1 Before you commit to the changes you need to add them to your staging area. Use the command

````
fossil add {file_name}
````
to add what you want to the staging area. If you want to add all the files that you altered, or all the new files, use the command

````
fossil add .
````
Now that we have all our files in the staging area, you can Commit the change with the command

````
fossil commit -m "{message}"
````
![](Final fossil/2 First Commit.jpg)

A big difference between FOSSIL and GIT is that in FOSSIl In the default configuration, the commit command will also automatically push your changes, but that feature can be disabled. So by doing the command above, you don't need to use 'Fossil push'.

After commit Fossil will also ask one last time if you really want to commit. You can disable this option. For now, just input 'a'.

####4 Fossil UI

Another big difference is that fossil has a web based ui. Use the command:

````
fossil ui
````

The ui command is intended for accessing the web user interface from a local desktop.

![](Final fossil/first commit ui.jpg)

####5 - Sync local repository

To Receive changes from others, use pull. Or go both ways at once using sync:

    fossil pull URL
    fossil sync URL 

When you pull in changes from others, they go into your repository, not into your checked-out local tree. To get the changes into your local tree, use update:

    fossil update VERSION 

####6 - Tickets

Issues, are called tickets in Fossil.

To create a ticket, first do the command 'fossil ui', then select the Tickets tab and select 'New Ticket'.

![](Final fossil/Create Ticket.jpg)

![](Final fossil/create ticket menu.jpg)

Now that you created a ticket, you can check their state :

![](Final fossil/ticket created.jpg)

Now create the readme.md file, create an issue, and then commit your modifications  to the remote repository.

Another difference in FOSSIL is that can't modify the state of a ticket in your commit message. First you commit your modifications, only then you alter the state of an issue with this command:

    Fossil ticket set {ticket id} status {new status}

![](Final fossil/close ticke.jpg)

As we did with GIT, create a ticket and close for all the tasks described here.


####7 - Tags

Now let's associate our first tag to our lsat commit. Tags work in a similar way as in GIT. Use the next command to create and add a tag to a commit:

     Fossil tag add {tag name} {status id}

Then, do the command fossil ui to check if the tags was correctly placed; 
    
![](Final fossil/create tag.jpg)

![](Final fossil/tag server.jpg)

Nicely done!


####8 - Let's practice - modify the source code of the project

For this example add jobYears field to employee. In this case, the jobYears shall be the difference between today and the first year, so make sure to implement that. After that, made some validations, test it and debug it. When all is done sync it with remote repository and add the tag v.1.2.0

````
fossil add .
fossil commit -m "adds jobyears"
Fossil tag add v1.2.0 3b6fafe528
````

Well done! This was the first part of our assignment. Mark your repository with the tag ca1-part1

````
Fossil tag add ca1-part1 3b6fafe528
````
####9 - Branches


Let's start the second part of our assignment. From now on we will use branches. This way another developer can continue to work on the main project while we also work on it. In FOSSIL you can only create a branch when you are committing, nd not before. So, first we will do some modifications in our source code. Add a new field, called email, do the validations, tests and debugs. Then commit the modifications, creating a branch called email-field.

````
Fossil add.
Fossil commit -m "Adds email" --branch email-field
````
![](Final fossil/branch creation.jpg)

You can check the branch created in the tab 'Timeline' in the FOSSIL UI. 

![](Final fossil/branches ui timeline.jpg)

It's time to merge it with our master branch (in this case it's called trunk). To merge two branches back together, first update to the branch you want to merge into. Then do a merge of the other branch that you want to incorporate the changes from.

    fossil update trunk
    fossil merge email-field
    # make sure the merge didn't break anything...
    fossil commit 

You can check the merge in the tab 'Timeline' in the FOSSIL UI.

![](Final fossil/merge ui.jpg)

Now let's add a tag

````
Fossil tag add v1.3.0 d2d2e1fd44
````

Let's do it again, to be sure we understood it. This time add a new validation to email. First of, create the branch, do the modifications, test it, debug it and then merge

````
'change source code'
 Fossil commit -m "Adds email validation" --branch fix-invalid-email
 Fossil update trunk
 Fossil merge fix-invalid-email
 Fossil commit 
````
That's it. We finished the second part of our assignment. Add a new tag

````
Fossil tag add v1.3.1 daab672ec0
Fossil tag add ca1-part2 daab672ec0
````
Congratulations! You can check the timeline of all your work in Fossil UI

![](Final fossil/print screen final.jpg)

#GIT vs FOSSIL - Analysis 

The feature sets of Fossil and Git overlap in many ways. Both are distributed version control systems which store a tree of check-in objects to a local repository clone. In both systems, the local clone starts out as a full copy of the remote parent. New content gets added to the local clone and then later optionally pushed up to the remote, and changes to the remote can be pulled down to the local clone at will. Both systems offer diffing, patching, branching, merging, cherry-picking, bisecting, private branches, a stash, etc. 

Major differences:

Both Fossil and Git store history as a directed acyclic graph (DAG) of changes, but Git tends to focus more on individual branches of the DAG, whereas Fossil puts more emphasis on the entire DAG.  For example, the default behavior in Git is to only synchronize a single branch, whereas with Fossil the only sync option is to sync the entire DAG. Git commands, GitHub, and GitLab tend to show only a single branch at a time, whereas Fossil usually shows all parallel branches at once.

Fossil favors sync over push: Explicit pushes are uncommon in Fossil-based projects: the default is to rely on autosync mode instead, in which each commit syncs immediately to its parent repository. This is a mode so you can turn it off temporarily when needed, such as when working offline. 

Unlike in Git, branch names in Fossil are not purely local labels. They sync along with everything else, so everyone sees the same set of branch names.

Private branches exist in Fossil, but they're normally used to handle rare exception cases, whereas in many Git projects, they're part of the straight-line development process.

Fossil's autosync system tries to keep each local clone identical to the repository it cloned from.

In my experience, we can do the same functionalities in Fossil, but with less commands. On the other side the time it takes to commit and push is a little longer in Fossil.

End.
