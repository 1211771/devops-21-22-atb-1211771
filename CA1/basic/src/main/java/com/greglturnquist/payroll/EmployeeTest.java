package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.testng.Assert.assertThrows;


class EmployeeTest {

    @org.junit.jupiter.api.Test
    void validateString_empty() {
        //Arrange
        String firstName = "";
        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, "lastname", "description", 5, "email@"));
    }

    @org.junit.jupiter.api.Test
    void validateString_null() {
        //Arrange
        String lastName = null;
        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("firstName",lastName, "description", 5,"email@"));
    }

    @org.junit.jupiter.api.Test
    void validateEmail() {
        //Arrange
        String email = "example";
        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("firstName","lastName", "description", 5,email));
    }

    @org.junit.jupiter.api.Test
    void validateJobYears_negative() {
        //Arrange
        int jobYears = -1;
        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Employee("firstName", "lastName", "description", jobYears,"email@"));
    }

    @org.junit.jupiter.api.Test
    void validateJobYears_zero() {
        //Arrange
        int jobYears = 0;
        //Assert
        assertDoesNotThrow(() -> new Employee("firstName", "lastName", "description", jobYears, "email@"));
    }

    @org.junit.jupiter.api.Test
    void validateNormalEmplyee() {
        //Assert
        assertDoesNotThrow(() -> new Employee("firstName", "lastName", "description", 1,"email@"));
    }

    @org.junit.jupiter.api.Test
    void calculateJobYears_zero() {
        // Arrange
        int startDate = 2022;
        int expected = 0;
        Employee employee = new Employee("example", "example", "example", 2,"email@");
        // Act
        int result = employee.calculateJobYears(startDate);
        // Arrange
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void calculateJobYears() {
        // Arrange
        int startDate = 2021;
        int expected = 1;
        Employee employee = new Employee("example", "example", "example", 2, "email@");
        // Act
        int result = employee.calculateJobYears(startDate);
        // Arrange
        assertEquals(expected, result);
    }
}