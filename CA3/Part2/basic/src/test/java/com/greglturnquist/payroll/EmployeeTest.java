package com.greglturnquist.payroll;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    String one_firstName;
    String one_lastName;
    String one_description;
    String one_jobTitle;
    String one_email;
    int one_jobYears;

    String two_firstName;
    String two_lastName;
    String two_description;
    String two_jobTitle;
    String two_email;
    int two_jobYears;


    @BeforeEach
    public void init() {

        // Default values employee 1
        one_firstName = "Frodo One";
        one_lastName = "Baggins";
        one_description = "ring bearer";
        one_jobTitle = "DevOps1";
        one_email = "email@email.pt";
        one_jobYears = 20;

        // Default values employee 2
        two_firstName = "Joe";
        two_lastName = "Doe";
        two_description = "example";
        two_jobTitle = "bot";
        two_email = "email@email.pt";
        two_jobYears = 5;


    }

    @Test
    void shouldReturnFalse_verifyIfFirstNameIsEmptyOrBlank() {
        // act & arrange
        IllegalArgumentException expected = assertThrows(
                IllegalArgumentException.class, () ->
                        new Employee("", one_lastName, one_description, one_jobTitle, one_jobYears, one_email)
        );
        String expectedMessage = expected.getMessage();

        // assert
        assertEquals(expectedMessage, "The field cannot be empty.");
    }

    @Test
    void shouldReturnFalse_verifyIfLastNameIsEmptyOrBlank() {
        // act & arrange
        IllegalArgumentException expected = assertThrows(
                IllegalArgumentException.class, () ->
                        new Employee(one_firstName, "", one_description, one_jobTitle, one_jobYears, one_email)
        );
        String expectedMessage = expected.getMessage();

        // assert
        assertEquals(expectedMessage, "The field cannot be empty.");
    }

    @Test
    void shouldReturnFalse_verifyIfDescriptionIsEmptyOrBlank() {
        // act & arrange
        IllegalArgumentException expected = assertThrows(
                IllegalArgumentException.class, () ->
                        new Employee(one_firstName, one_lastName, "", one_jobTitle, one_jobYears, one_email)
        );
        String expectedMessage = expected.getMessage();

        // assert
        assertEquals(expectedMessage, "The field cannot be empty.");
    }

    @Test
    void shouldReturnFalse_verifyIfJobTitleIsEmptyOrBlank() {
        // act & arrange
        IllegalArgumentException expected = assertThrows(
                IllegalArgumentException.class, () ->
                        new Employee(one_firstName, one_lastName, one_description, "", one_jobYears, one_email)
        );
        String expectedMessage = expected.getMessage();

        // assert
        assertEquals(expectedMessage, "The field cannot be empty.");
    }

    @Test
    void shouldReturnTrue_createNewEmployee() {
        // arrange
        // act
        Employee employeeOne = new Employee(one_firstName, one_lastName, one_description, one_jobTitle, one_jobYears, one_email);
        Employee employeeTwo = new Employee(one_firstName, one_lastName, one_description, one_jobTitle, one_jobYears, one_email);

        // assert
        assertEquals(employeeOne, employeeTwo);
    }

    @Test
    void shouldReturnTrue_createNewEmployeeHashCode() {
        // arrange
        // act
        Employee employeeOne = new Employee(one_firstName, one_lastName, one_description, one_jobTitle, one_jobYears, one_email);
        Employee employeeTwo = new Employee(one_firstName, one_lastName, one_description, one_jobTitle, one_jobYears, one_email);

        // assert
        assertEquals(employeeOne.hashCode(), employeeTwo.hashCode());
    }

    @Test
    void shouldReturnTrue_createNewEmployeeToString() {
        // arrange
        // act
        Employee employeeOne = new Employee(one_firstName, one_lastName, one_description, one_jobTitle, one_jobYears, one_email);

        // act
        String expected = "Employee{id=null, firstName='Frodo One', lastName='Baggins', description='ring bearer', jobTitle='DevOps1', jobYears='20', email='email@email.pt'}";
        // assert
        assertEquals(expected, employeeOne.toString());
    }

    @Test
    void shouldReturnFalse_createNewEmployee() {
        // arrange
        // act
        Employee employeeOne = new Employee(one_firstName, one_lastName, one_description, one_jobTitle, one_jobYears, one_email);
        Employee employeeTwo = new Employee(two_firstName, two_lastName, two_description, two_jobTitle, two_jobYears, two_email);

        // assert
        assertNotEquals(employeeOne, employeeTwo);
    }

    @Test
    void shouldReturnTrue_getEmployeeJobYears() {
        // arrange
        // one_jobYears = 20;
        Employee employeeOne = new Employee(one_firstName, one_lastName, one_description, one_jobTitle, one_jobYears, one_email);

        // act
        int expected = 20;

        // assert
        assertEquals(expected, employeeOne.getJobYears());
    }

    @Test
    void shouldReturnTrue_getEmployeeAttributes() {
        // arrange
        Employee employeeOne = new Employee(one_firstName, one_lastName, one_description, one_jobTitle, one_jobYears, one_email);

        // act
        // assert
        assertEquals(one_firstName, employeeOne.getFirstName());
        assertEquals(one_lastName, employeeOne.getLastName());
        assertEquals(one_description, employeeOne.getDescription());
        assertEquals(one_jobTitle, employeeOne.getJobTitle());
        assertEquals(one_jobYears, employeeOne.getJobYears());
    }

    @Test
    void shouldReturnTrue_getEmployeeEmail() {
        // arrange
        Employee employeeOne = new Employee(one_firstName, one_lastName, one_description, one_jobTitle, one_jobYears, one_email);
        employeeOne.setEmail("email@email.pt");

        // act
        // assert
        assertEquals("email@email.pt", employeeOne.getEmail());

    }

    @Test
    void shouldReturnFalse_verifyIfEmailIsEmptyOrBlank() {
        // act & arrange
        IllegalArgumentException expected = assertThrows(
                IllegalArgumentException.class, () ->
                        new Employee(one_firstName, one_lastName, "", one_jobTitle, one_jobYears, "")
        );
        String expectedMessage = expected.getMessage();

        // assert
        assertEquals("The field cannot be empty.", expectedMessage);

    }

    @Test
    void shouldReturnFalse_verifyIfEmailIsValid() {
        // act & arrange
        IllegalArgumentException expected = assertThrows(
                IllegalArgumentException.class, () ->
                        new Employee(one_firstName, one_lastName, one_description, one_jobTitle, one_jobYears, "email.email.com")
        );
        String expectedMessage = expected.getMessage();

        // assert
        assertEquals("The email is Invalid.", expectedMessage);
    }

    @ParameterizedTest
    @CsvSource(delimiter = ':',
            value = {
                    "user@domain.com:true",
                    "user@domain.co.in:true",
                    "user1@domain.com:true",
                    "user.name@domain.com:true",
                    "user_name@domain.co.in:true",
                    "user-name@domain.co.in:true",
                    "user@domain.com:true",
                    "us.do.co:false",
                    "us@do.co:true",
                    "@domaincom:false",
                    "d@d:false",
                    "d@d.c:false"
            })
    void isOdd_ShouldReturnTrueForOddNumbers(String input, boolean expected) {
        Employee employee = new Employee();
        //System.out.println(input+" "+ employee.checkEmail(input));
        assertEquals(expected, employee.checkEmail(input));
    }

}