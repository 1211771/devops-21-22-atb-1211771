#CA3-Part 2-Vagrant

We will now begin part 2 of CA3. This time, we will use Vagrant.
Vagrant isolates dependencies and their configuration within a single disposable and consistent development environment. Vagrant will, in this, case, work with Virtual machine and you will see that all necessary configuration is inside one file - VagrantFile.
Let us begin

###1 - Vagrant file

First download the vagrantfile in 

``
git clone https://1211771@bitbucket.org/atb/vagrant-multi-spring-tut-demo.git
``

Right now, this vagrant file is not ready for our application. We will need to do some modifications.
First, copy it to a directory called vagrant-box in Part2 of CA3.

First thing to do is change the java version. Our project was done in java 11, and the vagrant file is in java 8. In the 12th line of the file, write 

``
sudo apt-get install openjdk-11-jdk-headless -y
``

Next, we will need to find a box that works with java 11. You can check all available boxes in 

``
https://app.vagrantup.com/boxes/search
``

We will use one that is called 'hashicorp/bionic64'. We need to change the name of the box in three separate spots. In the configuration, line 4; in the first virtual machine, line 19; and in the second virtual machine, line 46

Next, in line 70, you need to put the link to clone your repository.

``
git clone https://1211771@bitbucket.org/1211771/devops-21-22-atb-1211771.git
``

In the next line, write the path to the root of the project. In our case, we copied the project to a directory called basic in part 2 of CA3

``
cd devops-21-22-atb-1211771/CA3/Part2/basic
``

That's all the alterations you need to do.

If you pay close attention, you wil see that this vagrant file is divided. First part is general configurations, second part is configurations for the first box 'db', third part configurations for the second box 'web', and lastly, the fourth part has the provisions. Provisions will only run in the first time that you are running the virtual machine unless you command it otherwise.

As stated above, this vagrant file creates 2 virtual boxes, One that will handle the data base, and the other that will handle all the rest.

###2 - changes to the project

We will also need to make some changes to our project.

Got to app.js file and add 'basic-0.0.1-SNAPSHOT' to the path in line 18.

Add a new class to payroll directory called 'ServeletInitializer.java'. Inside, write the next piece of code:

````
package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }

}

````

Now, go to application.properties in resources/templates and write the next piece of code:


````
spring.data.rest.base-path=/api
server.servlet.context-path=/basic-0.0.1-SNAPSHOT
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect

spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
````

Lastly, in build.gradle file, located in the root, add 

``
id 'war'
``

###3 - Create the virtual boxes

Open the command line and move to the directory where your vagrantfile is.
If you don't have a vagrant file, and wish to create one, you can type

``
vagrant init {name of the box}``

In our case, we have already a vagrant file, so all we need to type is

``
vagrant up --provision``

The '--provision' part is to force run the provisions.

Wait until it downloads and installs all it needs and then, if all went well, it will build the app without any problems.

You can open the VirtualBox software to see the virtual machines running;

![](Images/Sem Título.jpg)

Now, you can check the frontend with the url:

``
http://localhost:8080/basic-0.0.1-SNAPSHOT/
``

![](Images/Sem Título2.jpg)

You can also access directly the database and made modifications with the url

``
http://localhost:8082
``
![](Images/Sem Título3.jpg)

Congratulations, as you can see vagrant is very easy to use, once you have the vagrantfile ready!

End.

#Alternative - VMWare

In this CA, for the alternative, we will use VMWare instead of Oracle Virtual Box. VMWare is also a hypervisor that runs directly on server hardware without requiring an additional underlying operating system.

###1 - Install VMWare 

Go to the next link to install VMAare on your computer:

``
https://www.vmware.com/products/workstation-pro.html
``

Download it and install it. VMWare is a paid software, but you can download the trial version to see how it works.

###2 - Instal VMWare plug-in in vagrant

First we need to create the vagrant file. Copy the file created above and put it in a new directory named 'vagrant-vmware' in CA3/Part2

Now in the command line, go to the directory created and type:

``
vagrant plugin install vagrant-vmware-desktop
``

###3 - Install VMware Utilities

For VMWare to work in windows, you need to install VMWare utilities. Go to the link described bellow, download it and install it:

``
https://www.vagrantup.com/vmware/downloads
``

Now we are ready to use VMWare.

###4 - Make the necessary modifications in you vagrant file to work with VMWare

First we ned to change de general configurations. Right at the begining of your vagrant file, after the lines where you describe wich box you will use, type:

``
config.vm.provider "vmware_desktop"
config.vm.provider :vmware_desktop do |vmware|
vmware.vmx["ethernet0.pcislotnumber"] = "32"
end
``

Then, in the configurations for you 'web' virtual machine, in the section where you describe the RAM, substitute what was there for the next piece of code:

``
    web.vm.provider "vmware_desktop" do |v|
      v.vmx["memsize"] = 1024
      #v.vmx["numvcpus"] = "2"
    end
``

That's all de modifications that you need to do in your vagrant file.


###5 - With vagrant, run the virtual boxes

Now go to the correct directory and run the next command in the command line:

``
vagrant up --provision --provider=vmware_desktop
``

This will command vagrant to create the virtual boxes using VMWare. It will also command it to run the provision (quick reminder that the provision only run the first time you boot a virtual machine)

You can open the VMWare to see the virtual machines running:

![](Images/Sem Título 4.jpg)
You can also access directly the database and made modifications with the url

![](Images/Sem Título 5.jpg)


``
http://localhost:8082
``

![](Images/Sem Título 6.jpg)

Congratulations, as you can see vagrant is very easy to use, once you have the vagrantfile ready!

End.


#Differences between Oracle Virtual Box and VMWare:


Upon some investigations, we conclude that VMWare offers virtualization at the hardware level and virtual box offers virtualization at both hardware and software levels and both run on a multitude of operating systems.

Only virtual box is an open source tool, and, of course, VMWare has a paid subscription.

VirtualBox supports  VDI (Virtual Disk Image), Virtual Machine Disk (VMDK), Virtual Hard Disk (VHD) and VMWare only supports Virtual Machine (VMDK).

VirtualBox is a Type 2 Hypervisor, witch means that it creates the virtualization layer that separates the guest machine from the underlying operating system. VMware is a Type 1 Hypervisor that works directly on the host machine’s hardware resources.

In our case we noticed that VMWare seems to be faster getting the virtual boxes ready to run.

The end.

