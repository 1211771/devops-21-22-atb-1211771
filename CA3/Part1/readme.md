#CA3 - Virtualization

Virtualization pieces of software allow us to simulate a controlled environment within a computer. Almost like a computer inside another computer. It also will allow us to use multiple operating systems on a single machine. 

In this assignment we will work with the virtual box to create our first virtual machine and in the second part we will work with vagrant and see all the benefits that it offer.

----------------------------------
##Part 1 - Virtual Box

First we need to download virtual box. Virtual box is a free hypervisor that runs in many host operating systems, such
as Windows, Linux and OSX. you can download Virtual box in this link 


``````````
https://www.virtualbox.org/wiki/Downloads
``````````

### 1 - Create your first virtual machine


Open the Virtual box and select the blue icon 'New'. 
First we need to name our virtual machine. Let's name it DevOps-CA3-pART1.

Then select the operative system. In this case, it will be Linux Ubuntu (64 bits).

Then select the amount of memory (be carefully, it cannot go beyond the actual RAM of your physical computer). Select 2049 MB.

We will also simulate the hard disk, so select 'Create a Virtual Hard disk now', and then select VDI (VirtualBox Disk Image). So that only uses space that it fills, select 'Dynamically allocated'. Lastly, select the size of the hard disk. We will go with 10 GB.

### 2 - Install Ubuntu

Now we need to install our operative system. For that we need an ISO file. Go to the next link to download it:

``````
https://help.ubuntu.com/community/Installation/MinimalCD
```````````````
After downloading it, we need to add it to our machine. Go to settings, storage, and add the ISO file you just download. Then select 'LiveCD/DVD', so that it boots when you start your virtual machine.

Now Let's power on our virtual machine and follow the steps to install ubuntu.

First select 'Install'. Then select you language. We will choose Portuguese. Select you country (Portugal). 

Now select the option to detect your keyboard layout and follow the instructions. 

Next, select the name of your computer, we will select 'DevOps'. Then select again Portugal.

When asked about the proxy name, do not write anything.

Next, right the name of user. in our case it will be 'joao'. Then write the name of the account 'joaomarques' and choose a password.

Choose the disk you want ubuntu to be installed in and wait a little so that your virtual machine finishes installing ubuntu.

### 3 - Set network adapter

We need to create a connection between your physical computer and your virtual machine.

Turn off your virtual machine and take off the ISO ubuntu installation file . Now go  to settings, network, adapter 2, select 'Attached to Host-only Adapter and Name : VirtualBoxHost-Only Ethernet Adapter'. After starting the VM, log on into the VM and continue the setup:

Update the packages repositories:

`````````````
sudo apt update
`````````````

Install the network tools:

``
sudo apt install net-tools
``

Edit the network configuration file to setup the IP. For that we will use the editor nano:

``
sudo nano /etc/netplan/01-netcfg.yaml
``

make sure the contents of the file are similar to the following (in this case we are
setting the IP of the second adapter as 192.168.56.5):

``
network:
version: 2
renderer: networkd
ethernets:
enp0s3:
dhcp4: yes
enp0s8:
addresses:
192.168.56.5/24
``

Apply the new changes:

``
  sudo netplan apply
``

Install openssh-server so that we can use ssh to open secure terminal sessions to the VM (from other hosts)

``
  sudo apt install openssh-server
``

Enable password authentication for ssh

``
sudo nano /etc/ssh/sshd_config
``

Uncomment the line 'PasswordAuthentication yes' and apply the changes:

``
sudo service ssh restart
``

Install an ftp server so that we can use the FTP protocol to transfers files to/from the VM (from other hosts)

``  
sudo apt install vsftpd
``

Enable write access for vsftpd

``  sudo nano /etc/vsftpd.conf
``

Uncomment the line 'write_enable=YES' and apply the changes:

``
  sudo service vsftpd restar
``

###4 - Access your virtual machine from your physical computer

After you follow all this steps, you can now access the virtual machine terminal from you own computer. Just go to your own terminal and type (make sure your own virtual machine is running):

``
ssh joaomarques@192.168.56.5
``

As you can see you are inside the virtual machine. Isn't that awesome!

Now, we need to install all that we need to do the next exercises:

``
sudo apt install git;
sudo apt install openjdk-8-jdk-headless;
sudo apt install maven;
sudo apt install gradle;
``

###5 - Clone your repository and run all the assignment done until now

In the terminal of you virtual machine, or using the method described above, clone your repository like we learned in the first assignment:

``
git clone https://1211771@bitbucket.org/1211771/devops-21-22-atb-1211771.git
``

ok, now lets see if we can run all the functionalities that we have done up until now:

Go to basic directory in CA1 

``
cd CA1/basic
``

and run the app:

``
./mvnw spring-boot:run
``
(Note: You may be told that you have no execution permission to execute maven. If that's the case, use the command 'chmod +x mvnw', repeat everytime the error appears)

Now check http://192.168.56.5:8080/ and confirm that the correct table appears. Note : it's not localhost because the application is running in your virtual machine with its respective IP.

Next, let's go to CA2. In this case it's a little tricky. We will run the server in our virtual machine and the client in our physical machine. To that effect we need to do a little modification to our code. In the build.gradle file, in Part 1 of CA2, change the first arg of the task run client from 'localhost'  to the IP of your virtual machine, like '192.168.56.5'. With this modification our client will connect with the server in the written IP.

Now run your server in the virtual machine:

``
./gradlew runServer
``

And the clients in your physical machine:

``
./gradlew runClient
``

you can see that you can communicate  like we did in the second assignment. You can see the server running if you go to : 'http://192.168.56.5:8080/'

On to part 2 of the second assignment: This one is similar to CA1, but with gradle instead of maven. So, go to part2 of the second class assignment

``
cd 'CA2/part 2/react-and-spring-data-rest-basic'
``

and run the command line:

````
./gradlew build
````

you can check the frontend part of the application in 'http://192.168.56.5:8080/';

Congratulations! You have finished the first part of the assignment!




