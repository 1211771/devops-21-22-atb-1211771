# CA 2 - Build tools

Build tools are programs that automate the creation of executable applications from source code. Building incorporates
compiling, linking and packaging the code into a usable or executable form.

In this assignment we will learn how to work with Gradle, one of the most famous build tools.

Near the end, we will also do some exercises with Maven, so that we can directly compare the two build tools

---------------------------------

## Part 1

To begin this assignment create a CA2 directory. Then, create a Part1 directory. After the directories are created, it's
time to start.

For all tasks in this assignment, you should create an issue and, when you complete it, close it in the same commit. If
you have any questions about that, see the last assignment about version control pieces of software.

#### 1 - Clone the Project

1.1 -The first step is to clone the project to your local repository. You can do this writing the next command in the
command line inside the directory where you want to save it:

````
git clone https://1211771@bitbucket.org/luisnogueira/gradle_basic_demo.git
````

#### 2 - get to know the application

The project you just cloned is a simple task app. It will allow you to communicate with another people after you pick a
name that identifies you and is not chosen yet.

to be able to run the app, there are some requirements:

* Java JDK 8 (or newer)
* Apache Log4J 2
* Gradle 7.4.1 (if you do not use the gradle wrapper in the project)

This app uses gradle. To build a jar. file use the command:

````
./gradlew build 
````

After that lets start the server. Run the next command:

````
java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>
````

The server is running, but you cannot yet chat with other people. First you need to create a client. Open another
terminal and type:

````
./gradlew runClient
````

A new window will pop up, and you have to type a name. If you type a name that already exists, the app will not advance,
and it will ask you to type another name.

After you create a client, you can open another terminal, run the same command, create another client with a different
name and begin chatting. If different computers are connected in the same net and use the same server ip , they will be
able to chat with each other.

#### 3 - With gradle, add a new task to execute the server

If you open the file build.gradle, you will see that there already is a task to create a client. Now let's do on to run
the server. That is done with the next peace of code

````
task runServer(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches serverApp "

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerApp'

    args '59001'
}
````

In the task created, you can see that it belongs to the group "DevOps", has a description, the main class it refers to
is ChatServerApp and ha only one argument, that is the port being used.

To check if the task works, you can type in the terminal the next command and check if the terminal is running

````
./gradlew task run server
````

#### 4 - With gradle, add a simple unit test and update the gradle script to run it

In src directory, create a directory named Test and create a java file with the next peace of code:

````
    package basic_demo;

    import org.junit.Test;

    import static org.junit.Assert.*;

    public class AppTest {
        @Test
        public void testAppHasAGreeting() {
            App classUnderTest = new App();
            assertNotNull("app should have a greeting", classUnderTest.getGreeting());
        }
    }
````

Then, in Gradle.build add the dependency

````
   testImplementation 'junit:junit:4.12'
````

#### 5 - Add a new task of type Copy to be used to make a backup of the sources of the application

In build.gradle, type the next piece of code

````
task copyTask(type: Copy){
    from 'src'
    into 'backup'
}
````

run the task with the command

````
./gradlew task copytask
````

This task, called copyTask will copy all the content from src directory to the backup directory;

#### 6 - Add a new task of type Zip to be used to make an archive of the sources of the application.

In build.gradle, type the next piece of code

````
task ZipTask(type: Zip){
    from 'src'
    archiveName 'backup.zip'
    destinationDir(file('backup'))
}
````

This task, called ZipTask will copy the content from src file into a directory called backup in .zip format

run the task with the command

````
./gradlew task copytask
````

Congratulations, our first part of the assignment is done! tag the commit with ca2.part1

````
git tag ca2-part1

git push --tags
````

## Part 2

#### 7 - Create a new branch

First create 'Part2' directory. All work will be done there.

We will do the second part of the assignment in a different branch called tut-basic-gradle. Create the branch and move
there

````
git branch tut-basic-gradle
git checkout tut-basic-gradle
````

#### 8 - Start a new gradle spring boot project

Go to https://start.spring.io and start a new gradle spring boot project with the next information:

![](Documentation/Sem Título.jpg)

Instead of sprig boot 2.6.4, choose 2.6.6 version. Download is and unzip it in part 2 directory.

#### 9 - Copy the src folder from React.js and Spring Data RES Tutorial and paste it in gradle spring boot project

Just do as the title says. Also copy the webpack.config.js and package.json files.

Now we have React.js and Spring Data RES Tutorial using gradle instead of maven

#### 10 - Get the project to show frontend

You can see that the project, as it is, doesn't show anything in the localhost:8080 when it's running. To correct that
we need to add the next dependency in build.gradle file

````
id "org.siouan.frontend-jdk11" version "6.0.0"
````

Also add this code to the build.gradle:

````
frontend {
nodeVersion = "14.17.3"
assembleScript = "run build"
cleanScript = "run clean"
checkScript = "run check"
}
````

Lastly, Update the scripts section/object in package.json:

````
"scripts": {
"webpack": "webpack",
"build": "npm run webpack",
"check": "echo Checking frontend",
"clean": "echo Cleaning frontend",
"lint": "echo Linting frontend",
"test": "echo Testing frontend"
},
````

Now execute the command :

````
./gradlew build
````

You can now verify that the frontend part of the tutorial is working running the next command in the terminal and
checking localhost:8080

````
./gradlew bootRun
````

#### 11 - Add a task to gradle to copy the generated jar to a folder named "dist"

The jar. files are located in build/libs/ directory. Add the next peace of code to build.gradle

````
task copyTask(type: Copy) {
    from 'build/libs/'
    into 'dist/'
}
````

#### 12 - Add a task to gradle to delete all the files generated by webpack

This new task should be executed automatically by gradle before the task clean.

First add the new task to build.gradle:

````
task delete(type: Delete){
    delete 'src/main/resources/static/built'
````

Then, make the task created happen right before the clean task with the next piece of code:

````
clean {
    doFirst {
       delete
    }
}
````

Congratulations. That's the end of part 2! Tag your commit with ca2-part2

````
git tag ca2-part2

git push --tags
````

## Maven - alternative

We will now do the last few exercises with maven, so we can understand the differences.

First create the directory Part2_Maven and then copy the React.js and Spring Data RES Tutorial and paste it there.

#### 13 - Add a task to maven to copy the generated jar to a folder named "dist"

Maven don't work with tasks, but with goals.

First have to create the .jar files. Use the command

````
./mvnw install
````

Now, we need to add a plug in to the pom.xml file located in the basic directory. Add the next plug in:

````
            <plugin>
                <groupId>com.coderplus.maven.plugins</groupId>
                <artifactId>copy-rename-maven-plugin</artifactId>
                <version>1.0</version>
                <executions>
                    <execution>
                        <id>copy-file</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <configuration>
                            <sourceFile>target/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.jar</sourceFile>
                            <destinationFile>dist/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.jar</destinationFile>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
````

With this plug in we can copy and change the files name. Analysing the piece of code above, we can see that it will run in the 'generate-sources' phase when building the project. This is a big difference from Gradle. Here we have to decide in witch phase from the building process to run. 

With this plug in we copy the 'react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.jar' file located in the target directory and paste it in the dist directory. In this case, the created file will have the same name. 

Now, using the next command, we can check if the plug in worked:

````
./mvnw install
````

#### 14 - Add a task to maven to delete all the files generated by webpack

We will also do this task using a plug in. The plug in we will be using, alters the clean function so that we can add or remove directories or files to be clean. Type the next piece of code in the pom.xml file:

````
                        <plugin>
				<artifactId>maven-clean-plugin</artifactId>
				<version>3.2.0</version>
				<configuration>
					<filesets>
						<fileset>
							<directory>src/main/resources/static/built</directory>
							<includes>
								<include>bundle.js</include>
							</includes>
						</fileset>
					</filesets>
				</configuration>
			</plugin>
````

With this plug in we alter the clean function so that it also deletes the bundle.js in the built directory.
Lastly, we can type the next command to check if it really erases the chosen file:

````
./mvnw clean
````

Congratulations, you now ended part 2 with the maven alternative.

##Maven Vs Gradle

One of the main differences, as we saw early, is that Gradle is based on a graph of task dependencies – in which tasks are the things that do the work – while Maven is based on a fixed and linear model of phases. With Maven, goals are attached to project phases, and goals serve a similar function to Gradle’s tasks.

One of the disadvantages of Maven is that it uses xml language and when a project grows the pom.xml might as well be an unreadable XML file later on. Gradle uses a Groovy-based Domain-specific language(DSL) that is similar to javascript.

After some investigation, we can say that Gradle seems to be more flexible and performs better tha maven as it optimized for tracking only current running task.