#CA4-Part 1 - Docker

Welcome to the fourth assignment.

In the first part of this assignment we will talk about Docker. Docker is a piece of software that enables the use of virtualization through the creation of containers. These containers work like closed environments that are widely used to test applications.
It is a very powerful tool and at the same time, it's very easy to manage.

All the configuration that we will need to create a container is described in a dockerfile. This docker file, will create an image. You can see an image as a template used to build containers. From these images, we will build the containers. And inside the containers we can run any operative system with any app.

###1 - Download docker desktop

First we need to start by downloading and installing the Docker desktop software. Go to the link described next and download the appropriate version to you operative system:

``
https://docs.docker.com/desktop/windows/install/
``

Follow all the steps and install the software.

You can check witch version you have by typing in the command line :

``
docker --version
``

###2 - Building a dockerfile

DockerFile is a simple text file that is named "Dockerfile". Beware that the file title is case-sensitive. Here we will do all the configuration needed.

Normally, all Dockerfile's begin with 

``
FROM {*}  
``

Where * is an already existing Docker image, from witch our own image will be built upon.

As you can see inside CA4/Part1/Part1.1, the image that we will use will be 

``
openjdk:11
``

This image will come with java, version 11, already installed.

Next we will use commands to create and install everything that we need. In the end, our Dockerfile will look like this:

````
FROM openjdk:11

WORKDIR /app

RUN git clone https://bitbucket.org/luisnogueira/gradle_basic_demo.git

WORKDIR /app/gradle_basic_demo

RUN chmod u+x ./gradlew

RUN ./gradlew clean build

EXPOSE 59001

CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

````

The command Workdir creates a new directory (if it doesn't exist already) and, then moves your current working directory to the created directory.

The command Run executes a command;

So, going from top to bottom, we will create an image from openjdk:11, create and move to an app directory, clone the repository, move to gradle_basic:demo, give permissions to execute "gradlew" command, build the app using gradle, configure our port to be 5901 and lastly executes the server.

###3 - Create a container

The next command, when applied where in a directory where there is a Dockerfile, it will create an image with a name that you choose:

``
docker build -t gradle_app_devops .
``

In this case, tha name is gradle_app_devops. Now that the image is created, we will create the container with the command:

``
docker run  -d -p 59001:59001 gradle_app_devops
``

Wait a little, and then, if it all went according to plan, your server should be running. To check if its true let's try to create a client. In  your physical computer go to the directory  where the app is, build it, and run the client task:

````
gradlew clean build

java -cp basic_demo-0.1.0.jar basic_demo.ChatClientApp localhost 59001
````

Make sure that on your gradle.build file in the client port is 59001.

###4 - Upload the image created


Now that we have created an image, we can upload it to an online repository, so that is available for everyone.

To do this, first you need to create an account in Docker hub. Next, in your command line write:

````
docker login
````

Write your mail and password, then write:

````
docker tag gradle_app_devops:latest joaomarques17/dev_ops:app1
````

This will create a tag based on an image named 'gradle_app_devops', version 'latest' and prepare it to be uploaded to the 'joaomarques17/dev_ops' repository with the name 'app1';


Next, we only need to push it to the repository. Use the next command:

````
docker push joaomarques17/dev_ops:app1
````

This, will push the image app1 to the joaomarques17/dev_ops repository.

###5 -  Create another image, but this time, run the server on your computer

We will call this exercise 1.2. Our vagrant file will be in the directory CA4/Part1/Part 1.2.

Priorly we created and a container and run the server inside the container. Now we will run the server in our computer and then copy the .jar file created upon our build, past it in the container and run the server app.

This seems more complicated than it actually is. We just need to make some adjustments to our DockerFile.

in this case, our Dockerfile will look like this:

````
FROM tomcat:9

WORKDIR /app

COPY  basic_demo-0.1.0.jar .

EXPOSE 59001

CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

````

The differences are as follows: 

Instead of openjdk:11, we will build our image based on tomcat:9. This is possible because we are not building the app inside the container. We are just reading the .jar file. In this case we don't need java installed, only a server to run our app.

We don't need to clone any repository. Instead, we will copy the jar file that is located in the same directory as the Dockerfile (in this case CA4/Part1/Part 1.2) using the command copy.

Then, it's all the same, we will run the server using the port 59001.

As we did before, we can check if our server works, creatingg clients in our own computer using the command line:


````
gradlew clean build

java -cp basic_demo-0.1.0.jar basic_demo.ChatClientApp localhost 59001

````

To finish, let's put the created image in our dockerhub repository with the name app2:

````
docker login

docker tag gradle_app_devops2:latest joaomarques17/dev_ops:app2

docker push joaomarques17/dev_ops:app2
````

We did all our tasks for this part of the assignment through the command line. Nonetheless, we also can use the app Docker desktop. Let's just open it and check our 2 images:

![](../Images/Sem Título_dev.jpg)

The end

