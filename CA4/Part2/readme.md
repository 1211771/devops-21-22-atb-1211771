#CA4 - part2 - Docker-compose

In this part of the assignment we will work with the same piece of software - Docker. At the end, we will also take  a quick look to kubernetes.

We will add a new layer of complexity. In some situations we can have multiple containers connected in the same net. For example, an app can have its database in a container, it's backend in another container, and it's front end in another one.

Through a file called docker-compose, we can manage and modify the connection between our containers. This file does not substitute in any way, the Dockerfile necessary to create the images. We will need both.

### 1 Create the Dockerfiles's

In this exercise we will use an app that we also used in CA3/Part2. In one container, ,we will have the frontend/web part of the app and in the other we will have the database.

First, let's star with the database Dockerfile. It will look like this:

````
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-11-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

````

In this dockerFile we will create an image based from ubuntu image; install java -version 11, unzip and wget. These are all necessary pieces of software to run the app. Then we will create /usr/src/app directory and move there, get the .jar file necessary tu run the app; describe the ports and finally run the application.

Put this Dockerfile in the CA4/Part2/db directory.

Now, we need to create the frontend container. The DockerFile will look like this:

````
FROM tomcat:9.0.48-jdk11-openjdk-slim

RUN apt-get update -y 
RUN apt-get install -f
RUN apt-get install sudo git -y
RUN apt-get install sudo nodejs -y
RUN apt-get install sudo npm -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN  git clone https://1211771@bitbucket.org/1211771/devops-21-22-atb-1211771.git

WORKDIR /tmp/build/devops-21-22-atb-1211771/CA3/Part2/basic

RUN chmod u+x gradlew

RUN ./gradlew clean build

RUN cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

RUN rm -Rf /tmp/build/

EXPOSE 8080

````

In this dockerfile, we will create an image from tomcat:9.0.48-jdk11-openjdk-slim. This image already has a server and java installed. Then install all the necessary software (git, node-js, npm). Next we have a command to remove unnecessary files. We will also create and move to /tmp/build. Clone the app to our container, move to the directory where the gradle file is, give us permissions to execute the gradlew command; build the app and define the port to use.

Put this Dockerfile in the CA4/Part2/web directory.

###2 - Create the docker-compose file

This docker-compose file is a yml file. It will establish how the two containers will connect with each other. It will look like this: 

````
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.56.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data-backup
    networks:
      default:
        ipv4_address: 192.168.56.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.56.0/24
````

In this file, we can manage the ip and ports of the containers. We can also see that the web container depends on the db container. This means that the db container needs to be created first.

In this case, the docker-compose file need to be in the same directory as the web and db directories.

###3 - Create the images and the containers

In the command line, use the next command to build the images and the containers all at the same time:

````
docker-compose up
````

Wait a little, this can take some time. If all went well, you can check the docker desktop app to see the images:
![](../Images/Sem Título.png)

You can also open your browser and go to localhost:8008/basic-0.0.1-SNAPSHOT:

![](../Images/Sem Título part2 final.jpg)

###4 - Upload the new images

This will be similar to part1. As you already created an account on docker-hub , you just need to write these commands:

````
docker login

docker tag part2_db:latest joaomarques17/dev_ops:app1_part2
docker tag part2_web:latest joaomarques17/dev_ops:app2_part2

docker push --all-tags

````

###5 - Use a volume with the db container to get a copy of the database file 

In this  exercise, we will enter the container when its running and we will copy the database file to the  CA4/Part2/data directory.

To do this first, run the containers:

````
docker-compose start
````

Then use the next command to see the id fo the containers:

````
docker ps
````

Enter the container using the id:

````
docker exec -it {id} bash
````

And lastly copy the databasefile:

````
cp /usr/src/app/jpadb.mv.db /usr/src/data-backup
````

Now, you can see the fil in the CA4/Part2/data directory, that otherwise was empty.

Proof:

![](../Images/Sem Título finalissimo.jpg)

This is the end of the fourth class assignment. Congratulations!

#What is Kubernetes?

Kubernetes, also known as K8s, is an open source system for managing containerized applications across multiple hosts.

Containers group all necessary dependencies within one package. But to scale containers, you need a container orchestration tool—a framework for managing multiple containers.

Kubernetes architecture is, at its foundation, a client-server architecture.
The server side of Kubernetes is the known as the control plane. By default, there is a single control plane server that acts as a controlling node and point of contact. It scales containers horizontally. It ensures there are more or fewer containers available as the overall application's computing needs fluctuate.

The client side of Kubernetes comprises the cluster nodes—these are machines on which Kubernetes can run containers. These clusters are responsible to micromanage all the nodes.

Each cluster connects to a node through a kubelet process.

Inside the node there are pods. Pods are comprised of one or multiple containers located on a host machine, and the containers can share resources. Kubernetes finds a machine that has enough free compute capacity for a given pod and launches the associated containers. To avoid conflicts, each pod is assigned a unique IP address, which enables applications to use ports.

All the configuration of a cluster is in an App1.yml file.

### Kubernetes vs. Docker

Docker is not really a Kubernetes alternative, but newcomers to the space often ask what is the difference between them. The primary difference is that Docker is a container runtime, while Kubernetes is a platform for running and managing containers across multiple container runtimes. 

As stated above, if you need to handle the deployment of a large number of containers, networking, security, and resource provisioning become important concerns. Standalone Docker was not designed to address these concerns, and this is where Kubernetes comes in.

We won't apply kubernetes in our project as there are no need for it. This was only a research to get to know the technology and the solutions it brings.
