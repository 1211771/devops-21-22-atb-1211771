#CA5-Part2

This assignment will be  similar to the last one. We will now use the app in CA3-Part2 and dd some more stages to our pipeline.

###1 - Create the DockerFile

One of our new stages will create a docker image and publish it in docker hub. As we saw in one of the last assignments, to create a docker image we need a docker file. Ours is located in CA3/Part2 and it looks like this:

````
FROM tomcat:8-jdk11-temurin

RUN apt-get update -y

RUN apt-get install sudo nano git nodejs npm -f -y


RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://1211771@bitbucket.org/1211771/devops-21-22-atb-1211771.git

WORKDIR /tmp/build/CA3/Part2/basic

  ADD ./build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8085
````

To understand what a docker file does, please go see CA4.

Now it's time to create the pipeline

###2 - Creating a new pipeline

In this exercise, our jenkins file will be created through the jenkins interface, if you want to create a jenkinsFile and put it in your repository please check CA5-Part1 to see how it's done.

As stated before, in this pipeline, we will have new stages. For them to work we first need to install new plug ins. These will be 

````
Javadoc Plugin
HTML Publisher plugin
JUnit plugin
````
To install this plug ins, Go to 'Manage jenkins' option in your left menu and then select 'Manage Plugins'. Select the plug ins stated before and install them.

As stated before, in our new pipeline, we will publish images in dockerhub. as Dockerhub is protected by a log in, we need to add those credentials to Jenkins. To do that, go to 'manage Jenkins' options in the lef menu, then select 'Manage Credentials'. Select 'jenkins(global)' in Stores scoped to jenkins and then 'Global credentials(unrestricted)'. Lastly create the new credentials as the next image indicates:

![](../Part1/Images/4.jpg)

We are now, finally ready to create our JenkinsFile. As we didi in the last assignmente, select 'New Item', give it a name (DevOps_CA5_Part2_1211771) and select pipeline.

In the script portion, our script will look like this:

````
pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://1211771@bitbucket.org/1211771/devops-21-22-atb-1211771'
            }
        }
        stage('Assemble') {
            steps{
                echo 'Assembling...'
                dir ("CA3/Part2/basic"){
                    echo 'estou aqui'
                bat './gradlew assemble'
                }
            }
        }
        stage('Test'){
            steps {
                echo 'Testing...'
                dir ("CA3/Part2/basic"){
                bat './gradlew test'
                junit 'build/test-results/test/*.xml'
                }
            }
        }
        
        stage ('Javadoc'){
            steps{
                dir ("CA3/Part2/basic"){
                echo 'Publishing...'
                bat './gradlew javadoc'
                publishHTML([allowMissing: false,alwaysLinkToLastBuild: false,keepAll: false,reportDir: 'build/docs/javadoc',reportFiles: 'index.html',reportName: 'HTML Report', reportTitles: ''])
                }
            }
        }
        stage('Archiving'){
            steps{
                echo 'Archiving...'
                dir ("CA3/Part2/basic"){
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }
        stage('Docker Image') {
            steps {
                echo 'Publishing Docker Image...'
                dir('CA3/PART2/basic') {
                script {
                    docker.withRegistry('https://registry.hub.docker.com/', 'docker') {
                        def customImage=docker.build("joaomarques17/image:${env.BUILD_ID}")
                        customImage.push()
                    }
                }
            }
        }
    }}}
````

As you can see, we added two new stages 'Javadoc' - that will crete and publish documentation related to the app and 'Docker Image' that will create an image and publis it in DockerHub.

Now, let's see if it works! Build Now! (Beware that the last stage can take some time (7-10 minutes)

![](../Part1/Images/5.jpg)

Congratulations! You just ended the second assignment!

# Aternative -  Buddy

For the alternative to an