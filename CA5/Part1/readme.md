#CA5-Part 1 - Continuous integration tools - Jenkins

In our last assignment we will talk about continuous integration tools. Continuous integration (CI) is an agile and DevOps practice that enables multiple developers to contribute and collaborate in a shared code base at a rapid pace. Without continuous integration, developer collaboration is a tedious manual process of coordinating code updates and merges.

With  these tools, we can create a pipeline to do a specific set of tasks and works without manual inputs.

So let's start our assignment.

###1 - Install and run Jenkins

There are several ways to install jenkins. We will do it, downloading the .war file containing all the necessary information. Go to the link below

````
https://www.jenkins.io/download/
````

Now select the generic java package (.war).

Create a directory, in our case we called it "jenkins" and put the .war file there.

Through the command line, go to the created directory and execute the next command (this command will also be used to lunch jenkins):

````
java -jar jenkins.war
````

Follow all the necessary steps and install all recommended plug ins. Decide wich port jenkins will use when running (in our case it will be 8080). Select a username and a password and its done!

Now, everytime you want to run jenkins run the command described above and open 'localhost:8080' in you browser.

###2 - Creating a pipeline

Creating a pipeline in jenkins is relatively straightforward. All the information needed will be in a 'jenkinsFile'. This file can be created through the jenkins interface or it can be stored anywhere. In our first example, we will create a jenkins file through the jenkins interface.

We will create a pipeline for CA2, Part1 of our repository.

After jenkins is up and running, go to localhost:8080 in you repository, log in, and from the left menu choose the option 'New Item'.

Now choose a name, in our case it was DevOps_Ca5_Part1_1211771, and choose the option pipeline. Select OK and Jenkins will present you another page. Go to the end of that page and there you can see a Script field. Our script will be this:

````
pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://1211771@bitbucket.org/1211771/devops-21-22-atb-1211771'
            }
        }
        stage('Assemble') {
            steps{
                echo 'Assembling...'
                dir ("CA2/Part 1"){
                    echo 'estou aqui'
                bat './gradlew assemble'
                }
            }
        }
        stage('Test'){
            steps {
                echo 'Testing...'
                dir ("CA2/Part 1"){
                bat './gradlew test'
                junit 'build/test-results/test/*.xml'
                }
            }
        }
        stage('Archiving'){
            steps{
                echo 'Archiving...'
                dir ("CA2/Part 1"){
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }
}
````

You can see that our script is divided in stages. In the first stage checkout, we will order jenkins to clone our repository. 

In the second stage 'Assemble' we will go to the directory where is the app to run through the pipeline and we will run it through the command './(gradle assemble)'. 

In the third stage 'Test' we will run all the tests from  the application through the command './gradlew test'. 

Lastly, with the fourth stage we will export the app with the command 'archiveArtifacts 'build/distributions/*''

###3 - Run the pipeline

To run the pipeline created, go to Dashboard, choose the created pipeline and, from the left menu, choose 'Build Now'. Now just wait until all stages are successfully completed.

![](Images/1.jpg)

If you can see all stages in green, it means the pipelin worked with no problems

###4 - JenkinsFile in repository

We will now do the same exercise, but instead writing the necessary info to run the pipeline when creating the pipeline, we will have it in a JenkinsFile in the repository.

First we will create the jenkins file and put it in /CA5/Part1. You can see that our JenkinFile looks like this:

````
pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git url: 'https://1211771@bitbucket.org/1211771/devops-21-22-atb-1211771'
            }
        }
        stage('Assemble') {
            steps{
                echo 'Assembling...'
                dir ("CA2/Part 1"){
                    echo 'estou aqui'
                bat './gradlew assemble'
                }
            }
        }
        stage('Test'){
            steps {
                echo 'Testing...'
                dir ("CA2/Part 1"){
                bat './gradlew test'
                junit 'build/test-results/test/*.xml'
                }
            }
        }
        stage('Archiving'){
            steps{
                echo 'Archiving...'
                dir ("CA2/Part 1"){
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }
}
````

It is exactly the same as the one described in the last exercise.

Now, let's create another pipeline: Like ew did before, select new item, give it a name (DevOps_Ca5_Part1_1211771_JenkinsFile_in_Repo) and select 'pipeline'

Now we have to select some different options, in 'Definition', choose the option 'Pipeline Script from SCM'. in the SCM option, choose 'GIT', then write the url from you repo 'https://1211771@bitbucket.org/1211771/devops-21-22-atb-1211771.git'.

![](Images/2.jpg)

Lastly, in the script part choose the path to you jenkinsFile. In our case it is 'CA5/Part1/JenkinsFile'.

As we did before, build the pipeline and check if it all goes accord to plan.

![](Images/3.jpg)

Congratulations, you just ended CA5, Part1!

#Alternative - Buddy!

For the alternative to a continuous integration tool, we tried Buddy without success. We tried to log in with three different mails, but everytime we tried to connect, we received the next message
![](Images/Sem Título.png)

Nonetheless, we made some investigation and got to these conclusions:

Buddy seems to be a simpler and fast alternative to jenkins. It can be used through a web application and doesn't require installation of plug ins. Buddy seems to focus more on the "Continuous Deployment" aspect.

Jenkins, as it's not a web app, requires a proper installation.

With Buddy, you have two options to create a pipeline. Using the website or writing your buddy.yaml file. You can also generate a buddy.yaml file from a UI generated pipeline. Yaml is a well understood syntax that can be externally validated.

The interface for both of them are very similar as you can see the stages being completed and how many seconds it took to complete each stage.

One of the biggest differences is that buddy is a paid software. You can try it free for some limited time, but afterwards you have to have a paid license.

Jenkins offers mores in terms of customization as it doesn't support plug ins.

We hope you are ready to make the right choice with all information described above.

The end!